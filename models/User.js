const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({

	email: {
		type: String,
		required: [true, 'Enter your email address.']
	},

	password: {
		type: String,
		required: [true, 'Enter your password.']
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	orders: 
		[
			{
				productId: 
				{
					type: String,
					required: [true, 'Enter your order ID.']
				},

				totalAmount: {
					type: Number,
					
				},

				purchasedOn: {
					type: Date,
					default: new Date(),
				},

				status: {
					type: String,
					default: "purchased"
				}
			}
		]
})


module.exports = mongoose.model('User', userSchema)