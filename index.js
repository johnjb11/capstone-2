const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoutes = require("./routes/user")
const productRoutes = require("./routes/product")


const port = process.env.PORT || 3000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors())

mongoose.connect('mongodb+srv://admin:admin131@zuittbootcamp.tmxqb.mongodb.net/capstone2?retryWrites=true&w=majority', 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
	
	db.on("error", console.error.bind(console, "Connection Error"))

	db.once('open', () => console.log('Connected to the Mongo DB Atlas.'))

app.use("/users", userRoutes);	
app.use("/products", productRoutes);


app.listen(port, () => console.log(`Server is running at port ${port}.`))