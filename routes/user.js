const express = require("express");
const router = express.Router();
const userController = require("../controllers/user")
const auth = require("../auth")

//user registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController =>
		res.send(resultFromController))
})

//user authentication
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController =>
		res.send(resultFromController))

})



//make as admin
router.put('/:userId/setAsAdmin', auth.verify,(req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin === false) {
		userController.makeAsAdmin(req.params,req.body).then(resultFromController =>res.send("Changed status to ADMIN."))
	} 
	else {
		res.send("Only admin users can change admin functionality.")
	}

})



router.get("/allUsers", (req,res) =>{
	userController.getAllUsers().then(resultFromController =>
		res.send(resultFromController))
})

//ORDERS

//create order


router.post('/createOrder', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === false){
		userController.createOrder(req.body).then(resultFromController => res.send(resultFromController))
	} 
	else {
		return "Admin cannot create an order"
	}

})

//retrieve orders
router.get("/allOrders", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)
	
	if(userData.isAdmin === true){

	userController.getAllOrders().then(resultFromController =>
		res.send(resultFromController))
	
	} else {
		res.send("Cannot access. You are not an admin.")
	}
})

//retrieve single order
router.get('/getOrder', auth.verify, (req,res) =>{
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === false){

		userController.getOrder(userData.id).then(resultFromController => res.send(resultFromController))
	} 
	else {
		return "Cannot access. Authenticated users only."
	}

})







module.exports = router;

