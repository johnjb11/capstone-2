const express = require("express")
const router = express.Router();
const productController = require("../controllers/product")
const auth = require("../auth")




//retrieve all products
router.get("/all", (req,res) =>{
	productController.getAllProducts().then(resultFromController =>
		res.send(resultFromController))
})


//retrieve single product
router.get("/:productId", (req,res) => {
	console.log(req.params)
	console.log(req.params.productId)

	productController.getProduct(req.params).then(resultFromController =>res.send(resultFromController))
})


//create product
router.post("/create", auth.verify, (req,res)=>{
	

	if (auth.decode(req.headers.authorization))
			
		{
			productController.addProduct(req.body);
			res.send("Product created by admin.")
		} else {
			res.send("Authorized users only.")}
})




//update product
router.put("/:productId/update", auth.verify, (req,res) => {
	
	if (auth.decode(req.headers.authorization))

	{	
		productController.updateProduct(req.params, req.body)
		.then(resultFromController=>res.send(resultFromController))
	} else {
		res.send("Authorized users only.")}
})



//update product information
router.put('/:productId/archive', auth.verify,(req,res) => {

	if (auth.decode(req.headers.authorization))
	
		{productController.archiveProduct(req.params,req.body).then(resultFromController =>res.send(resultFromController))
	} 
	else {
		res.send("Authorization required to archive.")
	}

})


module.exports = router;

